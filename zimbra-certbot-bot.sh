#!/bin/bash

# Author: Florian Abensperg-Traun <flo@floriantraun.at>
# Version 1.2
# GPLv3 License

ZIMBRA_PATH="/opt/zimbra"
ZIMBRA_USER="zimbra"
CERTBOT_SERVER="https://acme-v02.api.letsencrypt.org/directory" # Use https://acme-staging.api.letsencrypt.org/directory for testing

function checkRoot() {
	if ((${EUID:-0} || "$(id -u)")); then
		report error "This script must be run as root."
		exit 1
	fi
}

function checkOS() {
	if [ ! "$(grep -Ei 'debian|buntu|mint' /etc/*release)" ]; then
		report error "This script only works on Debian based distros."
		exit 1
	fi
}

function report() {
	echo "[$1] $2"
}

function checkIfFileExists() {
	if [ ! -x "$1" ]; then
		report error "$1 not found"
		exit 1
	fi
}

function checkCertbotExecuteable() {
	CERTBOT_BIN=$(which certbot-auto certbot letsencrypt | head -n 1)

	if [ -z "$CERTBOT_BIN" ]; then
		echo "No letsencrypt/certbot binary found in $PATH"
		while true; do
			read -p "Do you want to install it automatically? (Y/n) " YN
			case $YN in
				[Yy]*)
					break
					;;
				[Nn]*)
					exit 1
					;;
				*)
					echo "Please answer Y or N"
			esac
		done

		add-apt-repository ppa:certbot/certbot
		apt-get update
		apt-get install software-properties-common
		apt-get install certbot

		# Run again to verify everything works
		checkCertbotExecuteable
	fi
}

function stopZimbra() {
	while true; do
		read -p "Zimbra services are going to be shut down. Do you want to continue? (Y/n) " YN
		case $YN in
			[Yy]*)
				break
				;;
			[Nn]*)
				exit 1
				;;
			*)
				echo "Please answer Y or N"
		esac
	done

	checkIfFileExists "$ZIMBRA_PATH/bin/zmcontrol"
	su - "$ZIMBRA_USER" -c "$ZIMBRA_PATH/bin/zmcontrol stop"
}

function requestCertificate() {
	checkIfFileExists "$ZIMBRA_PATH/bin/zmhostname"
	ZIMBRAHOSTNAME=$($ZIMBRA_PATH/bin/zmhostname)

	# Check domain
	while true; do
		read -p "Detected $ZIMBRAHOSTNAME as Zimbra domain. Use this hostname for certificate request? (Y/n) " YN
	    	case $YN in
				[Yy]* )
					DOMAINS=$ZIMBRAHOSTNAME
					break
					;;
				[Nn]* )
					break;
					;;
				* )
					echo "Please answer Y or N."
					;;
	    	esac
	done

	# If variable $DOMAINS is unset, ask for certificate domains
	if [ -z ${DOMAINS+x} ]; then
		read -p "Enter the domain(s). If you want to use multiple domains, seperate them with spaces: " DOMAINS
	fi

	# Explode $DOMAINS for certbot
	IFS=' ' read -ra EXPLODED <<< "$DOMAINS"
	DOMAINS=""
	for DOMAIN in "${EXPLODED[@]}"; do
		DOMAINS="$DOMAINS -d $DOMAIN"
	done

	# Check if ports 80 or 443 are used
	if [ "$(netstat -tuln | grep -Ei ':80 ')" ] || [ "$(netstat -tuln | grep -Ei ':443 ')" ]; then
		report error "Another webserver is running on port 80 or 443. Please shut it down and run this script again."
		exit 1
	fi

	read -p "Enter email address (used for urgent renewal and security notices): " EMAIL

	# Run certbot
	$CERTBOT_BIN certonly --standalone $DOMAINS --non-interactive --agree-tos --email "$EMAIL" --server "$CERTBOT_SERVER" | tee /tmp/certbot_output.txt

	# Grep certificate path
	CERTIFICATE_PATH="$(grep '/fullchain.pem' /tmp/certbot_output.txt)"
	rm /tmp/certbot_output.txt
	CERTIFICATE_PATH="$(echo  ${CERTIFICATE_PATH//'/fullchain.pem'})"

	if [ $? -ne 0 ] ; then
		report error "Let's Encrypt returned with an error."
		exit 1
	fi
}

function prepareCertificate() {
	if [ -z ${CERTIFICATE_PATH} ]; then
		report error "Certificates could not be retrieved."
		exit 1
	fi

	mkdir $ZIMBRA_PATH/ssl/letsencrypt 2>/dev/null
	cp $CERTIFICATE_PATH/* $ZIMBRA_PATH/ssl/letsencrypt/
	cp $ZIMBRA_PATH/ssl/letsencrypt/chain.pem $ZIMBRA_PATH/ssl/letsencrypt/zimbra_chain.pem
	chown $ZIMBRA_USER:$ZIMBRA_USER $ZIMBRA_PATH/ssl/letsencrypt/*

	# Add root CA to certificate chain
	cat << EOF >> $ZIMBRA_PATH/ssl/letsencrypt/zimbra_chain.pem
-----BEGIN CERTIFICATE-----
MIIDSjCCAjKgAwIBAgIQRK+wgNajJ7qJMDmGLvhAazANBgkqhkiG9w0BAQUFADA/
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
DkRTVCBSb290IENBIFgzMB4XDTAwMDkzMDIxMTIxOVoXDTIxMDkzMDE0MDExNVow
PzEkMCIGA1UEChMbRGlnaXRhbCBTaWduYXR1cmUgVHJ1c3QgQ28uMRcwFQYDVQQD
Ew5EU1QgUm9vdCBDQSBYMzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
AN+v6ZdQCINXtMxiZfaQguzH0yxrMMpb7NnDfcdAwRgUi+DoM3ZJKuM/IUmTrE4O
rz5Iy2Xu/NMhD2XSKtkyj4zl93ewEnu1lcCJo6m67XMuegwGMoOifooUMM0RoOEq
OLl5CjH9UL2AZd+3UWODyOKIYepLYYHsUmu5ouJLGiifSKOeDNoJjj4XLh7dIN9b
xiqKqy69cK3FCxolkHRyxXtqqzTWMIn/5WgTe1QLyNau7Fqckh49ZLOMxt+/yUFw
7BZy1SbsOFU5Q9D8/RhcQPGX69Wam40dutolucbY38EVAjqr2m7xPi71XAicPNaD
aeQQmxkqtilX4+U9m5/wAl0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNV
HQ8BAf8EBAMCAQYwHQYDVR0OBBYEFMSnsaR7LHH62+FLkHX/xBVghYkQMA0GCSqG
SIb3DQEBBQUAA4IBAQCjGiybFwBcqR7uKGY3Or+Dxz9LwwmglSBd49lZRNI+DT69
ikugdB/OEIKcdBodfpga3csTS7MgROSR6cz8faXbauX+5v3gTt23ADq1cEmv8uXr
AvHRAosZy5Q6XkjEGB5YGV8eAlrwDPGxrancWYaLbumR9YbK+rlmM6pZW87ipxZz
R8srzJmwN0jP41ZL9c8PDHIyh8bwRLtTcm1D9SZImlJnt1ir/md2cXjbDaJWFBM5
JDGFoqgCWjBH4d1QB7wCCZAA62RjYJsWvIjJEubSfZGL+T0yjWW06XyxV3bqxbYo
Ob8VZRzI9neWagqNdwvYkQsEjgfbKbYK7p2CNTUQ
-----END CERTIFICATE-----
EOF

	# Verifing the certificates
	su - $ZIMBRA_USER -c "$ZIMBRA_PATH/bin/zmcertmgr verifycrt comm $ZIMBRA_PATH/ssl/letsencrypt/privkey.pem $ZIMBRA_PATH/ssl/letsencrypt/cert.pem $ZIMBRA_PATH/ssl/letsencrypt/zimbra_chain.pem"

	if [ $? -ne 0 ] ; then
		report error "Somethin went wrong."
		exit 1
	fi
}

function deployCertificate() {
	cp -a $ZIMBRA_PATH/ssl/zimbra $ZIMBRA_PATH/ssl/zimbra.$(date "+%Y%.m%.d-%H.%M")
	cp $ZIMBRA_PATH/ssl/letsencrypt/privkey.pem $ZIMBRA_PATH/ssl/zimbra/commercial/commercial.key

	su - $ZIMBRA_USER -c "$ZIMBRA_PATH/bin/zmcertmgr deploycrt comm $ZIMBRA_PATH/ssl/letsencrypt/cert.pem $ZIMBRA_PATH/ssl/letsencrypt/zimbra_chain.pem -deploy all"
}

function startZimbra() {
	su - "$ZIMBRA_USER" -c "$ZIMBRA_PATH/bin/zmcontrol start"
}

# --------------------------------------------------------------------------------

function main() {
	case $1 in
		newCertificate*)
			checkRoot
			checkOS
			checkCertbotExecuteable
			stopZimbra
			requestCertificate
			prepareCertificate
			deployCertificate
			startZimbra
			;;
		renewCertificate*)
			#
			;;
		*)
			report error "An error occured."
			exit 1
	esac
}

# Arguments passed?
if [ $# != 0 ]; then
	if [ $1 == "-r" ]; then
		main renewCertificate
	else
		main newCertificate
	fi
fi

main newCertificate
